<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Libro;
use Illuminate\Support\Facades\DB;

class LibrosController extends Controller
{
    public function index(Request $request)
    {
        $libros = Libro::all();
        
        return view('libros.index', compact('libros'));
    }


    public function create(Request $request)
    {
        
        return view('libros.create');
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'titulo' => 'required',
            'editorial' => 'required',
            
        ]);

        try{
            Libro::create($request->all());
            return redirect()->route('libros.index')->with('msn_success', 'Operacion Satisfactoria !!!');

        }catch(\Exception $e){

            

            $fechaHoraActual = date("Y-m-d H:i:s");
            $mensaje=$fechaHoraActual.' No se puede crear el registro';

            return redirect()->route('libros.create')->with('msn_error', $mensaje);
        }
    }



    public function edit($id) {
        $libro = Libro::findOrFail($id);
        return view('libros.edit', compact('libro'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'editorial' => 'required',
        ]);
    
        try {
            DB::beginTransaction();
            $libro = Libro::findOrFail($id);
    
            $libro->update([
                'titulo' => $request->titulo,
                'editorial' => $request->editorial,
            ]);
    
            DB::commit();
            return redirect()->route('libros.index')->with('msn_success', 'Curso actualizado exitosamente.');
        } catch (\Exception $e) {
            DB::rollback();
            // LogHelper::logError($this, $e);
    
            $fechaHoraActual = date("Y-m-d H:i:s");
            return redirect()->route('libros.edit', $libro->id)->with('msn_error', $fechaHoraActual . ' Ocurrió un error al actualizar el curso.');
        }
    }

}
