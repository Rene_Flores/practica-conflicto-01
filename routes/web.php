<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LibrosController;

Route::get('/generamos', [LibrosController::class, 'index'])->name("libros.index");
Route::get('/', [LibrosController::class, 'index'])->name("libros.index");


Route::get('libros/create', [LibrosController::class, 'create'])->name("libros.create");
Route::post('libros', [LibrosController::class, 'store'])->name('libros.store');

Route::get('libros/{idlibros}/edit', [LibrosController::class, 'edit'])->name("libros.edit");
Route::put('libros/{idlibros}', [LibrosController::class, 'update'])->name('libros.update');

