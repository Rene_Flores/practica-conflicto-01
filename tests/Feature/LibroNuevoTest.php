<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LibroNuevoTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    use RefreshDatabase;
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_Libro_store_success(): void
    {
        
        
        $libroData = [
            'titulo' => 'aaa',
            'editorial' => 'bbb',
            
        ];
        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.index'));
        $response->assertSessionHas('msn_success', 'Operacion Satisfactoria !!!');

        $this->assertDatabaseHas('libros', [
            'titulo' => 'aaa',
            'editorial' => 'bbb',
            
        ]);
        
    }

    public function test_Libro_store_exception(): void
    {


        $nombre="";
        for($i=0;$i<255;$i++){
            $nombre.=" aaa";
        }

        $libroData = [
            'titulo' => $nombre,
            'editorial' => 'bbb',
            
        ];
        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.create'));
        //
        $response->assertSessionHas('msn_error');

        $mensajeErrorSession = session('msn_error');
        $mensajeError= "No se puede crear el registro";
        $this->assertStringContainsString($mensajeError,$mensajeErrorSession);

        $this->assertDatabaseMissing('libros',$libroData);

        
    }

    public function test_Libro_store_validation(): void
    {
        $libroData = [
            'titulo' => '',
            'editorial' => '',
        ];
        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'titulo',
            'editorial',
        ]);
        $this->assertDatabaseMissing('libros',$libroData);
       
    }

}
