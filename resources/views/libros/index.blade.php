@extends('layout')    
@section('content')

<div class="container">
   <a href="{{ route('libros.create') }}" class="btn btn-primary">Crear Libro</a>
<table class="table" border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Editorial</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($libros as $libro)
            <tr>
                <td>{{ $libro->id }}</td>
                <td>{{ $libro->titulo }}</td>
                <td>{{ $libro->editorial }}</td>
                <td>
                    <a href="{{ route('libros.edit', ['idlibros' => $libro->id]) }}" class="btn btn-primary">Editar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if(session('msn_success'))

<script>

  let mensaje="{{ session('msn_success') }}";

  Swal.fire({

    icon:"success",

    html: `<span style="font-size: 16px;">${mensaje}</span>`,

  });

</script>

@endif


@endsection